import React from 'react';
import { Route, Switch } from 'react-router-dom'
import AboutUs from './pages/about-us'
import ContactUs from './pages/contact-us'
import CurriculumDesign from './pages/curriculum-design'
import ExamsAndAssessments from './pages/exams-and-assessments'
import Home from './pages/home'
import SmartTechnology from './pages/smart-technology'
import TeachersDevelopment from './pages/teachers-development'
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Container from '@material-ui/core/Container';
import Fab from '@material-ui/core/Fab';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Zoom from '@material-ui/core/Zoom';
import Navbar from './components/navbar/index';
import Footer from './components/footer';
import scrollToTop from './components/scroll-to-top';
import ParentCommunication from './pages/parent-communication';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'fixed',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
}));

function ScrollTop(props) {
  const { children, window } = props;
  const classes = useStyles();
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
    disableHysteresis: true,
    threshold: 100,
  });

  const handleClick = (event) => {
    const anchor = (event.target.ownerDocument || document).querySelector('#back-to-top-anchor');

    if (anchor) {
      anchor.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  };

  return (
    <Zoom in={trigger}>
      <div onClick={handleClick} role="presentation" className={classes.root}>
        {children}
      </div>
    </Zoom>
  );
}

ScrollTop.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default function Routes(props) {
  return (
    <>
    <scrollToTop />
      <CssBaseline />
      <AppBar style={{boxShadow:'none'}}>
        <Navbar />
      </AppBar>
      <Toolbar id="back-to-top-anchor" />
      <Container style={{padding:'0px'}}>
        <Switch>
            <Route path='/' exact render={()=><Home />} />
            <Route path='/about-us' exact render={()=> <AboutUs />} />
            <Route path='/contact-us' exact render={()=><ContactUs />} />
            <Route path='/smart-technology' exact render={()=><SmartTechnology />} />
            <Route path='/curriculum-design' exact render={()=><CurriculumDesign />} />
            <Route path='/teachers-development' exact render={()=><TeachersDevelopment />} />
            <Route path='/exams-and-assessments' exact render={()=><ExamsAndAssessments />} />
            <Route path='/parent-communication' exact render={()=><ParentCommunication />} />
        </Switch>
      </Container>
      <div>
        <Footer />
      </div>
      <ScrollTop {...props}>
        <Fab color="default" size="small" aria-label="scroll back to top">
          <KeyboardArrowUpIcon />
        </Fab>
      </ScrollTop>
    </>
  );
}
