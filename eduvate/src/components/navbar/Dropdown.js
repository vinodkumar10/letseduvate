import React, { useState , useEffect} from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import './Dropdown.css';

function Dropdown() {

  const [data, setData] = useState([]);

  useEffect(() => {
    axios.get('http://localhost:8080/services_menu')
    .then(res=>{
      console.log(res)
      setData(res.data)
    })
    .catch((err)=>console.log(err))
  }, [])

  const [click, setClick] = useState(false);

  const handleClick = () => setClick(!click);

  return (
    <>
      <ul style={{width:'300px'}}
        onClick={handleClick}
        className={click ? 'dropdownMenu clicked' : 'dropdownMenu'}
      >
        {data.map((item, index) => {
          return (
            <li key={index}>
              <Link
                className={item.cName}
                to={item.path}
                onClick={() => setClick(false)}
              >
                {item.title.toUpperCase()}
              </Link>
            </li>
          );
        })}
      </ul>
    </>
  );
}

export default Dropdown;
