import React ,{useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import MenuIcon from '@material-ui/icons/Menu';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSortDown } from '@fortawesome/free-solid-svg-icons';
import EduvateLogo from '../../assests/Eduvate-Logo-1.png';
import Dropdown from './Dropdown';
import './styles.css';
import MobileDropDown from './MobileDropDown';

function Navbar() {
const [click, setClick] = useState(false);
const [dropdown, setDropdown] = useState(false);
const [mobileDropDown, setMobileDropDown] = useState(false);

const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined
  });

useEffect(() => {
    if(window.innerWidth > 768){
        setMobileDropDown(false);
    }
}, [mobileDropDown])

const handleClick = () => {
    setClick(!click)
    setMobileDropDown(false)
};
const onMouseEnter = () => {
    if (window.innerWidth < 800) {
        console.log("vinod")
      setDropdown(false);
    } else {
      setDropdown(true);
      console.log("vinod else")
    }
  };

  const onMouseLeave = () => {
    if (window.innerWidth < 800) {
        console.log("vinod_2")
      setDropdown(false);
    } else {
        console.log("vinod_2 else")
      setDropdown(false);
      setMobileDropDown(!mobileDropDown)
    }
  };

const handleMobileDropDown = () =>{
    if(window.innerWidth < 768){
        console.log("more witdh")
        setMobileDropDown(!mobileDropDown)
    }
    else{
        console.log("more witdh")
        setMobileDropDown(false)
    }
}
    return (
        <div>
            <div className="navbar__menu">
                <div className="navbar_logo">
                    <Link to='/'><img src={EduvateLogo} alt="eduvate_logo"/></Link>
                </div>
                <div className="toggle_button" onClick={handleClick}>
                    <MenuIcon className="menu_icon" style={{fontSize:'35px'}}/>
                </div>
                <div className={click ? "nav_links_active active" : "nav_links"}>
                    <ul className="nav_menu">
                        <li className="nav_item">
                            <Link to='/' className="nav_link">Home</Link>
                        </li>
                        <li className="nav_item">
                            <Link to='/about-us' className="nav_link">about us</Link>
                        </li>
                        <li className="nav_item" 
                            onMouseEnter={onMouseEnter}
                            onMouseLeave={onMouseLeave}
                            onClick={handleMobileDropDown}
                        >
                            <Link className="nav_link">
                                services 
                                <FontAwesomeIcon icon={faSortDown} style={{margin:'2px'}}/> 
                            </Link>
                            {dropdown && <Dropdown />}
                            {mobileDropDown && <MobileDropDown />}
                        </li>
                        <li className="nav_item">
                            <Link to="contact-us" className="nav_link">contact us</Link>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Navbar;