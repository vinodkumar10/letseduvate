import React , {useState, useEffect}from 'react'
import { Link } from 'react-router-dom';
import axios from 'axios';
import './MobileDropDown.css';

function MobileDropDown() {
  const [data, setData] = useState([]);

  useEffect(() => {
    axios.get('http://localhost:8080/services_menu')
    .then(res=>{
      setData(res.data)
    })
    .catch((err)=>console.log(err))
  }, [])

    return (
        <>
          <ul style={{width:'300px', fontSize:'12px'}}
            className="dropdown-content"
          >
            {data.map((item, index) => {
              return (
                <li key={index}>
                  <Link
                    className="dropdown-content-li"
                    to={item.path}
                  >
                    {item.title.toUpperCase()}
                  </Link>
                </li>
              );
            })}
          </ul>
        </>
  
    )
}

export default MobileDropDown;
