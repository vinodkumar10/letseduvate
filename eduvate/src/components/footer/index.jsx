import React from 'react';
import './styles.css';
import { FaFacebookF } from 'react-icons/fa';
import { FaInstagram } from 'react-icons/fa';

function Footer() {
    return (
        <div>
            <footer>
    <div class="upper-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-12 social-sec">
                    <img src="https://letseduvate.com/wp-content/uploads/2021/01/Eduvate-Logo.png" alt="" srcset="" />
                    <ul class="social-icons size-sm">
                        <li><a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/letseduvate/">
                                <FaFacebookF className="social_icon facebook" />
                            </a>
                        </li>
                        <li><a target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/letseduvate2020/">
                                <FaInstagram className="social_icon instagram" />
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 col-xs-12">
                    <h3>
                        About Us
                    </h3>
                    <p>
                    Eduvate is not a business but a mission to empower educators,students and parents. We create value for all stakeholders in the school ecosystem by providing custom solutions for helping schools to simplify operations, train teachers, design curriculum, easily manage examinations and streamline parent communication. 

                    </p>

                </div>
                <div class="col-md-4 col-xs-12">
                </div>
            </div>
        </div>
    </div>
    <div class="footer-container">
        <p>Copyright &copy; K12 Techno Services Pvt. Ltd</p>
    </div>
</footer>

        </div>
    )
}

export default Footer;
