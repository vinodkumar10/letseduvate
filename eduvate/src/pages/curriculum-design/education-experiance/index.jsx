import React from 'react'
import './styles.css';

function EducationalExperiance() {
    return (
        <div>
            <div class="services">
            <div class="container">
                <h2>
                    Enhancing the Educational Experience of Every Stakeholder
                </h2>
                <div class="row service-wrapper">
                    <div class="col-xs-12">
                            <img src="https://letseduvate.com/wp-content/uploads/2021/01/Chart.png" alt="" srcset="" />
                    </div>
                </div>
            </div>            
        </div>
        </div>
    )
}

export default EducationalExperiance;
