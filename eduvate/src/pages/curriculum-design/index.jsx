import React from 'react';
import EduCtaAboutUs from '../about-us/edu-cta/index';
import EducationalExperiance from './education-experiance';
import './styles.css';

function CurriculumDesign() {
    return (
        <>
            <div class="edu-banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                    <h1>A Comprehensive Curriculum That<br />  Blends the Best of Teaching<br />
                        <span> Methodologies and Teaching Aids </span>
                    </h1>
                    </div>
                    <div class="col-md-6 col-xs-12">
                    <video class="video-intro" controls>
                        <source src="https://letseduvate-website.s3.ap-south-1.amazonaws.com/Marketing+Collateral-+LetsEduvate/1.+Integrated+Curriculum.mp4" type="video/mp4" />
                    </video>                        
                    </div>
                </div>
            
            </div>
        </div>
        <div>
            <EduCtaAboutUs />
        </div>
        <div>
            <EducationalExperiance />
        </div>
        </>
    )
}

export default CurriculumDesign;
