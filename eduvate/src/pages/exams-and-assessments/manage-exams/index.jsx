import React from 'react'
import './styles.css';

function ManageExams() {
    return (
        <div>
            <div class="services">
            <div class="container">
                <h2>
                    Effortlessly Manage Exams
                </h2>
                <div class="row service-wrapper">
                    <div class="col-md-6 col-xs-12">
                        <div class="service-card">
                            <img src="https://letseduvate.com/wp-content/uploads/2021/01/1a1.png" alt="" srcset="" />
                            <h4>Type of Exams</h4>
                            
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="service-card">
                            <img src="https://letseduvate.com/wp-content/uploads/2021/01/2a2.png" alt="" srcset="" />
                            <h4>Periodic weekly &amp; monthly tests</h4>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="service-card">
                            <img src="https://letseduvate.com/wp-content/uploads/2021/01/3a2.png" alt="" srcset="" />
                            <h4>Progressive assessments</h4>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="service-card">
                            <img src="https://letseduvate.com/wp-content/uploads/2021/01/4a.png" alt="" srcset="" />
                            <h4>Descriptive Assessments</h4>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        </div>
    )
}

export default ManageExams;
