import React from 'react';
import EduCtaAboutUs from '../about-us/edu-cta/index';
import ManageExams from './manage-exams';
import './styles.css';

function ExamsAndAssessments() {
    return (
        <>
            <div class="edu-banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                    <h1>All in One Assessment Tool  <br />
                        <span>to Track All Parameters</span>
                    </h1>
                    </div>
                    <div class="col-md-6 col-xs-12">
                    <video width="100%" height="350px" controls>
                        <source src="https://letseduvate-website.s3.ap-south-1.amazonaws.com/Marketing+Collateral-+LetsEduvate/2.+Conducting+Examinations.mp4" type="video/mp4" />
                    </video>                        
                    </div>
                </div>
            </div>
        </div>
        <div>
            <EduCtaAboutUs />
        </div>
        <div>
            <ManageExams />
        </div>
        </>
    )
}

export default ExamsAndAssessments;
