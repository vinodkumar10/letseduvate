import React from 'react';
import './styles.css';

function TeachersTechnology() {
    return (
        <div>
            <div class="services">
            <div class="container">
                <h2>
                    Our Technologies is designed to enhance
                </h2>
                <div class="row service-wrapper">
                    <div class="col-md-4 col-xs-12">
                        <div class="service-card">
                            <img src="https://letseduvate.com/wp-content/uploads/2021/01/1.png" alt="" srcset="" />
                            <h4>Overview</h4>
                            <p style={{color: '#7d7f80', fontSize:'15px'}}>Detailed plan on how to teach a chapter and the relevant teaching aids to be used</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="service-card">
                            <img src="https://letseduvate.com/wp-content/uploads/2021/01/2a1.png" alt="" srcset="" />
                            <h4>Lesson plans</h4>
                            <p style={{color: '#7d7f80',fontSize:'15px'}}>Subject/Chapter/Period wise intricately designed resources</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="service-card">
                            <img src="https://letseduvate.com/wp-content/uploads/2021/01/3a1.png" alt="" srcset="" />
                            <h4>Teaching aids</h4>
                            <p style={{color: '#7d7f80', fontSize:'15px'}}>Each plan is supplemented with teaching aids such as videos and presentations for crystal clear understanding</p>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        </div>
    )
}

export default TeachersTechnology;
