import React from 'react';
import EduCtaAboutUs from '../about-us/edu-cta/index';
import './styles.css';
import TeachersTechnology from './teachers-technology';

function TeachersDevelopment() {
    return (
        <>
            <div class="edu-banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                    <h1>Transforming teaching techniques <br /> 
                        <span>for better understanding.</span></h1>
                    </div>
                    <div class="col-md-6 col-xs-12">
                    <video class="video-intro" controls>
                        <source src="https://letseduvate-website.s3.ap-south-1.amazonaws.com/Marketing+Collateral-+LetsEduvate/5.+Teacher_s+Management.mp4" type="video/mp4" />
                    </video>                        
                    </div>
                </div>
            </div>
        </div>
        <div>
            <EduCtaAboutUs />
        </div>
        <div>
            <TeachersTechnology />
        </div>
        </>
    )
}

export default TeachersDevelopment;
