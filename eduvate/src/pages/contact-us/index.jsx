import React from 'react'
import './styles.css';
import Mail from '../../assests/envelope-solid.svg';
import Phone from '../../assests/phone-alt-solid.svg';

function ContactUs() {
    return (
        <di>
            <section class="edu-home">
	<div class="edu-banner about-us-banner">
		<div class="container">
		    <div class="row">
		    	<div class="col-md-6 col-xs-12">
		            <div class="service-section-content contact-details">
		            	<h1>Contact Us</h1>
		                <p>
		                 <a href="mailto:Sales@letseduvate.com">
                            <img src={Mail} alt="" width="20px" height="20px" style={{marginRight:'5px'}}/>  Sales@letseduvate.com</a><br />
		                <a href="tel:+916364899689"><img src={Phone} alt="" width="20px" height="20px" style={{marginRight:'5px'}} /> +91 6364 899 689</a><br />
		                Address : <br />
		                 2nd Floor, No.70, HMT Main Road, <br />
		                9th Main Road, OPP. SBM, Mathikere,<br />
		                 Mathikere, Bangalore.
		                </p>
		                
		            </div>
		        </div>
		        <div class="col-md-6 col-xs-12 edu-form">
                <form>
                        <label id="errormsg" style={{color: 'red'}} class="text-danger"></label>
                            <div class="form-group">
                                <label>Your Name</label>
                                <input placeholder="Name*" required="" type="text" class=" form-control" id="name" name="name" value="" data-type="text" aria-required="true" aria-labelledby="fld_8768091Label" />

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Phone</label>
                                <input placeholder="Phone Number*" maxlength="10" required="" type="text" oninput="numberOnly('phone_num');" class="form-control" id="phone_num" name="phone" value="" data-type="text" aria-required="true" aria-labelledby="fld_8768091Label" />
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input placeholder="Email ID" type="text" class="form-control" id="email" name="email" value="" data-type="text" required="" />
                            </div>
                            <div class="form-group">
                                <label>Your Message</label>
                               <textarea class="form-control" rows="3" id="msg"></textarea>
                            </div>
                            <div class="button-wrapper">
                            <button type="submit" id="letsForm" value="Submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
    	        </div>


		        <div class="col-md-12">

		        		        </div>
		    </div>
		</div>
	</div>
</section>
        </di>
    )
}

export default ContactUs
