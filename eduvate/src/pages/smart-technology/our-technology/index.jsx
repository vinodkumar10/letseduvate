import React from 'react';
import './styles.css';

function OurTechnology() {
    return (
        <div>
            <div class="services">
            <div class="container">
                <h2>
                    Our Technologies is designed to enhance
                </h2>
                <div class="row service-wrapper">
                    <div class="col-md-3 col-xs-12">
                        <div class="service-card">
                            <img src="https://letseduvate.com/wp-content/uploads/2021/01/1a.png" alt="" srcset="" />
                            <h4>Administrative Operations</h4>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12">
                        <div class="service-card">
                            <img src="https://letseduvate.com/wp-content/uploads/2021/01/2a.png" alt="" srcset="" />
                            <h4>Online Classes</h4>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12">
                        <div class="service-card">
                            <img src="https://letseduvate.com/wp-content/uploads/2021/01/3a.png" alt="" srcset="" />
                            <h4>Tracking Student Performance</h4>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12">
                        <div class="service-card">
                            <img src="https://letseduvate.com/wp-content/uploads/2021/01/3a.png" alt="" srcset="" />
                            <h4>Tracking teachers’ performance basis students’ performance</h4>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        </div>
    )
}

export default OurTechnology;
