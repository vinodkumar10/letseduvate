import React from 'react'
import EduCtaAboutUs from '../about-us/edu-cta'
import OurTechnology from './our-technology'

function SmartTechnology() {
    return (
        <>
            <div class="edu-banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                            <h1>Introducing the <br /> smarter way<br />
                            <span>to manage the school</span></h1>
                    </div>
                    <div class="col-md-6 col-xs-12">
                    <video class="video-intro" controls>
                                <source src="https://letseduvate-website.s3.ap-south-1.amazonaws.com/Marketing+Collateral-+LetsEduvate/6.+Technology.mp4" type="video/mp4" />
                            </video>                        
                    </div>
                </div>
            
            </div>
        </div>
        <div>
                <EduCtaAboutUs />
        </div>
        <div>
            <OurTechnology />
        </div>
        </>
    )
}

export default SmartTechnology
