import React from 'react';
import './styles.css';
function EduChallengeTransform(){
    return (
        <div>
            <div class="edu-challenges transform">
            <div class="container">
                <h2>Transforming the life of thousands</h2>
                <div class="solution-card-container row">
                    <div class="solution-card col-md-6 col-xs-12">
                        <div class="icon-wrapper">
                        
                        <img src="https://letseduvate.com/wp-content/uploads/2021/01/Student-icon1.png" alt="icon" srcset="" />
                        </div>
                        <h3><strong>25000+</strong><br /> Students</h3>
                    </div>
                    <div class="solution-card col-md-6 col-xs-12">
                        <div class="icon-wrapper">
                        <img src="https://letseduvate.com/wp-content/uploads/2021/01/teacher-icon1.png" alt="icon" srcset="" />
                        </div>
                        <h3><strong>3000+</strong><br /> Teachers</h3>
                    </div>
                </div>
            </div>
        </div>
        </div>
    )
}

export default EduChallengeTransform;
