import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Slider from "react-slick";
import './styles.css';

export default class Services extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        }
    ]
    };
    
    return (
      <div className="con">
        <link rel="stylesheet" type="text/css" charset="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
        <style>{cssstyle}</style>
            <h2>
                Services
            </h2>
        <Slider {...settings}>
            <div>
                <Link to="/teachers-development/">
                    <div class="service-card">
                        <img src="https://letseduvate.com/wp-content/uploads/2021/01/1a.png" alt="" srcset="" />
                        <h4>Teachers Development</h4>
                    </div>
                </Link>
            </div>
            <div>
                <Link to="/smart-technology/">
                    <div class="service-card">
                        <img src="https://letseduvate.com/wp-content/uploads/2021/01/2a.png" alt="" srcset="" />
                        <h4>Technology Support</h4>
                    </div>
                </Link>
            </div>
            <div>
                <Link to="/curriculum-design/">
                    <div class="service-card">
                        <img src="https://letseduvate.com/wp-content/uploads/2021/01/3a.png" alt="" srcset="" />
                        <h4>Curriculum Design</h4>
                    </div>
                </Link>
            </div>
            <div>
                <Link to="/exams-and-assessments/">
                    <div class="service-card">
                        <img src="https://letseduvate.com/wp-content/uploads/2021/01/1a1.png" alt="" srcset="" />
                        <h4>Exams and Assessments</h4>
                    </div>
                </Link>
            </div>
            <div>
                <Link to="/parent-communication/">
                    <div class="service-card">
                        <img src="https://letseduvate.com/wp-content/uploads/2021/01/Easy-Login.png" alt="" srcset="" />
                        <h4>Parent Communication</h4>
                    </div>
                </Link>
            </div>
                
        </Slider>
      </div>
    );
  }
}

const cssstyle = `
.con {
  margin: 0 auto;
  padding: 0px 40px 40px 40px;
//   width: 400px;
//   border:1px solid black;
}
.con h2 {
        text-align: center;
        text-transform: uppercase;
        color: #7d7f80;
        font-size: 2em;
        font-weight: 500;
}
.slick-next:before, .slick-prev:before {
    color: #000;
    // border: 1px solid red;
}
.slick-slider{
    // border: 1px solid blue;
    // padding:30px;
} 
.slick-slide{
    // border:1px solid red;
    width:100%;
    margin:auto;
    text-align: center;
}
`