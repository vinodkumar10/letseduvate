import React from 'react'
import './styles.css';
import Slider from "react-slick";

function Challenges() {
    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
      };
    return (
        <div>
            <div class="edu-challenges">
            <div className="container">
            <h2>Challenges &amp; Solution</h2>
                <Slider {...settings}>
                <div class="solution-card-container row">
                    <div className="slider-challenge-position">
                    <div class="solution-card col-md-5 col-xs-12">
                        <div class="icon-wrapper"><img src="https://letseduvate.com/wp-content/uploads/2021/01/Ineffective-Mnagment-Tool.png" srcset="" alt="icon" /></div>
                        <h3>Ineffective Management Tools</h3>
                        </div>
                        <div class="arrow-container col-md-1 col-xs-12">
                        <p><i class="arrow right"></i></p>
                        <p><i class="arrow down"></i></p>
                        </div>
                        <div class="solution-card col-md-6 col-xs-12">
                        <div class="icon-wrapper"><img src="https://letseduvate.com/wp-content/uploads/2021/01/Efficient-ERP.png" srcset="" alt="icon" /></div>
                        <h3>Efficient ERP</h3>
                    </div>
                    </div>
                </div>
                <div class="solution-card-container row">
                    <div className="slider-challenge-position">
                    <div class="solution-card col-md-5 col-xs-12">
                        <div class="icon-wrapper"><img src="https://letseduvate.com/wp-content/uploads/2021/01/2-Lack-of-trained-teachers.png" srcset="" alt="icon" /></div>
                        <h3>Lack of Trained Teachers</h3>
                        </div>
                        <div class="arrow-container col-md-1 col-xs-12">
                        <p><i class="arrow right"></i></p>
                            <p><i class="arrow down"></i></p>
                        </div>
                        <div class="solution-card col-md-6 col-xs-12">
                        <div class="icon-wrapper"><img src="https://letseduvate.com/wp-content/uploads/2021/01/2-Upskilling-of-teachers.png" srcset="" alt="icon" /></div>
                        <h3>Upskilling of Teachers</h3>
                    </div>
                    </div>   
                </div>
                <div class="solution-card-container row">
                   <div className="slider-challenge-position">
                   <div class="solution-card col-md-5 col-xs-12">
                    <div class="icon-wrapper"><img src="https://letseduvate.com/wp-content/uploads/2021/01/3-Outdated-teaching-methology.png" srcset="" alt="icon" /></div>
                    <h3>Outdated Teaching</h3>
                    </div>
                    <div class="arrow-container col-md-1 col-xs-12">
                    <p><i class="arrow right"></i></p>
                        <p><i class="arrow down"></i></p>
                    </div>
                    <div class="solution-card col-md-6 col-xs-12">
                    <div class="icon-wrapper"><img src="https://letseduvate.com/wp-content/uploads/2021/01/3-teaching-Kits.png" srcset="" alt="icon" /></div>
                    <h3>Interactive Kits</h3>
                    </div>
                   </div>
                </div>
                <div class="solution-card-container row">
                    <div className="slider-challenge-position">
                    <div class="solution-card col-md-5 col-xs-12">
                        <div class="icon-wrapper"><img src="https://letseduvate.com/wp-content/uploads/2021/01/4-Tedious-Assessment-Process.png" srcset="" alt="icon" /></div>
                        <h3>Tedious Assessment</h3>
                        </div>
                        <div class="arrow-container col-md-1 col-xs-12">
                        <p><i class="arrow right"></i></p>
                        <p><i class="arrow down"></i></p>
                        </div>
                        <div class="solution-card col-md-6 col-xs-12">
                        <div class="icon-wrapper"><img src="https://letseduvate.com/wp-content/uploads/2021/01/4-Evaluation.png" srcset="" alt="icon" /></div>
                        <h3>Simplified Evaluation</h3>
                    </div>
                    </div>
                </div>
                    <div class="solution-card-container row">
                        <div className="slider-challenge-position">
                        <div class="solution-card col-md-5 col-xs-12">
                            <div class="icon-wrapper">
                            
                            <img src="https://letseduvate.com/wp-content/uploads/2021/01/5-Parent-Communication.png" alt="icon" srcset="" />
                            </div>
                            <h3>Incoherent Parent Communication</h3>
                        </div>
                        <div class="arrow-container col-md-1 col-xs-12">
                            <p><i class="arrow right"></i></p>
                            <p><i class="arrow down"></i></p>
                        </div>
                        <div class="solution-card col-md-6 col-xs-12">
                            <div class="icon-wrapper">
                            <img src="https://letseduvate.com/wp-content/uploads/2021/01/5-Multi-Channel-Communication.png" alt="icon" srcset="" />
                            </div>
                            <h3>Multi Channel Connectivity </h3>
                        </div>
                        </div>
                    </div>
                </Slider>
            </div>
            </div>
        </div>
    )
}

export default Challenges;
