import React from 'react';
import './styles.css';
import TestomonialLogo from '../../../assests/Testimonial.png';
function Testominial() {
    return (
        <div>
            <div class="testimonial">
            <h2>
                Don't just take our word, See what teachers and students are saying
            </h2>
            <div class="container">
                <div class="testi-card">
                    <div class="test-img">
                        <img src={TestomonialLogo} alt="" srcset="" />
                    </div>
                    <div class="test-content">
                        <h3>R. Krishnamurthi</h3>
                        <h5>Principal</h5>
                        <p>We, at Orchids, believe in nurturing the child in a way that they enjoy the learning process. Although traditional teaching methods are at the core, we have evolved our teaching techniques to make our students future-ready. With an engaging curriculum and student-centric approach, we focus on making the child self-confident and ready to take on the world.</p>
                    </div>
                </div>
            </div>
        </div>
        </div>
    )
}

export default Testominial
