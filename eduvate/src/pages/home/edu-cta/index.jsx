import React from 'react'
import './styles.css';
function EduCta() {
    return (
        <div>
            <div class="edu-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-xs-12">
                        <h2>See How Eduvate Helps Schools to Overcome Growth Challenges</h2>
                            <video class="video-intro" controls>
                                <source src="https://letseduvate-website.s3.ap-south-1.amazonaws.com/Marketing+Collateral-+LetsEduvate/LetsEduvate+Introduction+Video.mp4" type="video/mp4" />
                            </video>
                    </div>
                    <div class="col-md-4 col-xs-12 edu-form">
                        <h2>Enquire Now</h2>
                        <form style={{boxShadow:'none'}}>
                        <label id="errormsg" style={{color: 'red'}} class="text-danger"></label>
                            <div class="form-group">
                                <label>Your Name</label>
                                <input placeholder="Name*" required="" type="text" class=" form-control" id="name" name="name" value="" data-type="text" aria-required="true" aria-labelledby="fld_8768091Label" />

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Phone</label>
                                <input placeholder="Phone Number*" maxlength="10" required="" type="text" oninput="numberOnly('phone_num');" class="form-control" id="phone_num" name="phone" value="" data-type="text" aria-required="true" aria-labelledby="fld_8768091Label" />
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input placeholder="Email ID" type="text" class="form-control" id="email" name="email" value="" data-type="text" required="" />
                            </div>
                            <div class="form-group">
                                <label>Your Message</label>
                               <textarea class="form-control" rows="3" id="msg"></textarea>
                            </div>
                            <div class="button-wrapper">
                            <button type="submit" id="letsForm" value="Submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        </div>
    )
}

export default EduCta;
