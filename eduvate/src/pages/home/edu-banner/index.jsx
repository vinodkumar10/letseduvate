import React from 'react'
import './styles.css';
function EducationBanner() {
    return (
        <div>
            <div class="edu-banner">
            <div style={{padding:'0px 70px'}}>
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <h1>A Complete <br/>
                            <span>School Growth Partner</span></h1>
                            <p>Eduvate fastracks school growth by custom solutions to optimize operational efficiency, training and curriculum design.</p>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <img src="https://letseduvate.com/wp-content/uploads/2021/01/Banner-Illustration.png" alt="banner" srcset=""  className="banner_img"/>
                    </div>
                </div>
            </div>
        </div>
        </div>
    )
}

export default EducationBanner;
