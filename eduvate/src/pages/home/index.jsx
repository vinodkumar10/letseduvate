import React from 'react'
import EducationBanner from './edu-banner'
import EduChallengeTransform from './edu-challenge-transform'
import Challenges from './edu-challenges'
import EduCta from './edu-cta'
import Services from './services'
import Testominial from './testominial'

function Home() {
    return (
        <div>
            {/* <h1>Home Page</h1> */}
            <div>
                <EducationBanner />
            </div>
            <div>
                <EduCta />
            </div>
            <div>
                <Challenges />
            </div>
            <div style={{padding:'20px'}}>
                <Services />
            </div>
            <div>
                <EduChallengeTransform />
            </div>
            <div>
                <Testominial />
            </div>
        </div>
    )
}

export default Home;
