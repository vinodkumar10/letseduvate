import React from 'react'
import './styles.css';

function EduCtaAboutUs() {
    return (
        <div>
            <div class="edu-cta">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="service-section-content">
                            <p about-text="" style={{color:'#666'}}>
                            Gone are the days of tedious, traditional hard work. Pave way for an efficient management software to transform your existing academic system with Eduvate! While you focus on celebrating learning, we take care of routine organisational tasks.
                            </p>
                            
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12 edu-form">
                        <form>
                            <label id="errormsg" style={{color: 'red'}} class="text-danger"></label>
                            <div class="form-group">
                                <label>Your Name</label>
                                <input placeholder="Name*" required="" type="text" class=" form-control" id="name" name="name" value="" data-type="text" aria-required="true" aria-labelledby="fld_8768091Label" />

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Phone</label>
                                <input placeholder="Phone Number*" maxlength="10" required="" type="text" oninput="numberOnly('phone_num');" class="form-control" id="phone_num" name="phone" value="" data-type="text" aria-required="true" aria-labelledby="fld_8768091Label" />
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input placeholder="Email ID" type="text" class="form-control" id="email" name="email" value="" data-type="text" required="" />
                            </div>
                            <div class="form-group">
                                <label>Your Message</label>
                                <textarea class="form-control" rows="3" id="msg"></textarea>
                            </div>
                            <div class="button-wrapper">
                            <button type="submit" id="letsForm" value="Submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
    )
}

export default EduCtaAboutUs;
