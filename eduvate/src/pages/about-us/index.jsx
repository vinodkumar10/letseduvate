import React from 'react';
import EduCtaAboutUs from './edu-cta/index';
import './styles.css';

function AboutUs() {
    return (
        <div>
            <div class="edu-banner about-us-banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12 about-us-banner-title">
                        <h1>About Us</h1>
                    </div>
                    <div class="col-md-6 col-xs-12">
                    <p style={{fontSize:'17px'}}>
                    Eduvate is not a business but a mission to empower educators,students and parents. We create value for all stakeholders in the school ecosystem by providing custom solutions for helping schools to simplify operations, train teachers, design curriculum, easily manage examinations and streamline parent communication.
                    </p>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <EduCtaAboutUs />
        </div>
        </div>
    )
}

export default AboutUs;
