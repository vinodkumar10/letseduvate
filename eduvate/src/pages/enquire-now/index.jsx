import React from 'react';
import './styles.css';

const EnquireNow = () => {
    return (
        <>
            {/* <h1>Enquire Now</h1> */}
            <div class="col-md-4 col-xs-12 edu-form">
                <h2>Enquire Now</h2>
                <form>
                    <label id="errormsg"  class="text-danger"></label>
                    <div class="form-group">
                        <label>Your Name</label>
                        <input placeholder="Name*" required="" type="text" class=" form-control" id="name" name="name" value="" data-type="text" aria-required="true" aria-labelledby="fld_8768091Label" />

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Phone</label>
                        <input placeholder="Phone Number*" maxlength="10" required="" type="text" oninput="numberOnly('phone_num');" class="form-control" id="phone_num" name="phone" value="" data-type="text" aria-required="true" aria-labelledby="fld_8768091Label" />
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input placeholder="Email ID" type="text" class="form-control" id="email" name="email" value="" data-type="text" required="" />
                    </div>
                    <div class="form-group">
                        <label>Your Message</label>
                        <textarea class="form-control" rows="3" id="msg"></textarea>
                    </div>
                    <div className="button-wrapper">
                    <button type="submit" id="letsForm" value="Submit" className="btn btn-primary">Submit</button>
                    </div>
                </form>
                </div>


        </>
    )
}

export default EnquireNow;

