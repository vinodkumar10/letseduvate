import React from 'react';
import EduCtaAboutUs from '../about-us/edu-cta';
import SpecialApps from './special-apps';

function ParentCommunication() {
    return (
        <>
            <div class="edu-banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                            <h1>Establishing effective<br />
                            <span>Parent Communication</span></h1>
                    </div>
                    <div class="col-md-6 col-xs-12">
                    <video class="video-intro" controls>
                        <source src="https://letseduvate-website.s3.ap-south-1.amazonaws.com/Marketing+Collateral-+LetsEduvate/4.+Seamless+Parent+Communication.mp4" type="video/mp4" />
                    </video>                        
                    </div>
                </div>
            
            </div>
        </div>
        <div>
                <EduCtaAboutUs />
        </div>
        <div>
            <SpecialApps />
        </div>
        </>
    )
}

export default ParentCommunication;
