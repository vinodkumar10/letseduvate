import React from 'react';

function SpecialApps() {
    return (
        <div>
            <div class="services">
            <div class="container">
                <h2>
                    The specially developed app offers
                </h2>
                <div class="row service-wrapper">
                    <div class="col-md-4 col-xs-12">
                        <div class="service-card">
                            <img src="https://letseduvate.com/wp-content/uploads/2021/01/Easy-Login.png" alt="" srcset="" />
                            <h4>Easy login and navigation</h4>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="service-card">
                            <img src="https://letseduvate.com/wp-content/uploads/2021/01/Trackiing-activites.png" alt="" srcset="" />
                            <h4>Tracking of daily activities</h4>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="service-card">
                            <img src="https://letseduvate.com/wp-content/uploads/2021/01/Track-Attendance.png" alt="" srcset="" />
                            <h4>Access to attendance reports, marksheets etc</h4>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        </div>
    )
}

export default SpecialApps;
