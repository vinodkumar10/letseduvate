import './App.css';
import ScrollToTop from './components/scroll-to-top';
import Routes from './routes';

function App() {
  return (
    <div className="App" style={{backgroundColor:'#fff'}}>
      {/* <h1>testing</h1> */}
      <ScrollToTop />
      <Routes />
    </div>
  );
}

export default App;
