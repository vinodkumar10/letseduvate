const servicesMenu = [
    {
        title: 'Smart Technology',
        path: '/smart-technology',
        cName: 'dropdown-link'
      },
      {
        title: 'Curriculum-Design',
        path: '/curriculum-design',
        cName: 'dropdown-link'
      },
      {
        title: 'teachers development',
        path: '/teachers-development',
        cName: 'dropdown-link'
      },
      {
        title: 'exams and assessments',
        path: '/exams-and-assessments',
        cName: 'dropdown-link'
      }
]

module.exports = servicesMenu;