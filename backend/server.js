const express = require("express");
const cors = require("cors");
const services_menu = require("./services_menu");

const app = express();

app.use(express.json());
app.use(cors());

app.get('/services_menu', (req, res)=>{
    res.status(200).json(services_menu)
})

app.post('/services_menu', (req, res)=>{
    const title = req.body.title;
    const path = req.body.path;
    const cName = req.body.cName;

    const serveces = {title, path, cName}
    services_menu.push(serveces);
    res.status(200).json({message: "Data Added Succesfully"})
})

app.listen(8080, ()=>{
    console.log("The server is up and runnig")
})
